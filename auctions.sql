/*
 Navicat Premium Data Transfer

 Source Server         : mysqlconnection
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : auctions

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 22/05/2018 00:18:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auction
-- ----------------------------
DROP TABLE IF EXISTS `auction`;
CREATE TABLE `auction`  (
  `AuctionID` int(255) NOT NULL AUTO_INCREMENT,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Slot` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `State` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `LotID` int(255) NOT NULL,
  `UserID` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`AuctionID`) USING BTREE,
  INDEX `fk_userid`(`UserID`) USING BTREE,
  INDEX `fk_lotid`(`LotID`) USING BTREE,
  CONSTRAINT `fk_lotid` FOREIGN KEY (`LotID`) REFERENCES `lot` (`LotNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_userid` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lot
-- ----------------------------
DROP TABLE IF EXISTS `lot`;
CREATE TABLE `lot`  (
  `LotNo` int(255) NOT NULL AUTO_INCREMENT,
  `Category` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Description` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `State` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ReservePrice` int(255) NOT NULL,
  `HammerPrice` int(255) NULL DEFAULT NULL,
  `Comission` int(255) NULL DEFAULT NULL,
  `CustomerPayment` int(255) NULL DEFAULT NULL,
  `AuctionID` int(255) NULL DEFAULT NULL,
  `UserID` int(11) NULL DEFAULT NULL,
  `CurrentBid` int(255) NULL DEFAULT NULL,
  `LatestBidBy` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`LotNo`) USING BTREE,
  INDEX `AuctionID`(`AuctionID`) USING BTREE,
  INDEX `lot_userid`(`UserID`) USING BTREE,
  CONSTRAINT `lot_ibfk_1` FOREIGN KEY (`AuctionID`) REFERENCES `auction` (`AuctionID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `lot_userid` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `UserID` int(255) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(2000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Email` varchar(2000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `UserType` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `UserName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  INDEX `Id`(`UserID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Procedure structure for AuctionAdd
-- ----------------------------
DROP PROCEDURE IF EXISTS `AuctionAdd`;
delimiter ;;
CREATE PROCEDURE `AuctionAdd`(`LotID` int)
BEGIN
	#Routine body goes here...

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for NewProc
-- ----------------------------
DROP PROCEDURE IF EXISTS `NewProc`;
delimiter ;;
CREATE PROCEDURE `NewProc`(IN `LotID` int)
BEGIN
	#Routine body goes here...

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table auction
-- ----------------------------
DROP TRIGGER IF EXISTS `auction_trigger`;
delimiter ;;
CREATE TRIGGER `auction_trigger` AFTER INSERT ON `auction` FOR EACH ROW BEGIN
    UPDATE auctions.lot SET State='Assigned',AuctionID=(SELECT AuctionID from auctions.auction where LotID=(SELECT LotID from auctions.auction ORDER BY LotID DESC LIMIT 1)) where LotNo=(SELECT LotID from auctions.auction ORDER BY LotID DESC LIMIT 1);
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
