/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction;
import java.sql.*;
import java.io.*;
import java.sql.Date;
import javax.swing.JOptionPane;
/**
 *
 * @author Nischal Prajapati
 */
public class AddAuctionTrigger {
    public static void Add (Date StartDate,Date EndDate,String Slot,int LotID, String State,int uid)
  throws SQLException {
    GlobalConnection db=new GlobalConnection();
    String sql = "INSERT into auction (StartDate,EndDate,Slot,LotID,State,UserID) values (?,?,?,?,?,?)";;
    try {
      PreparedStatement pstmt =db.conn.prepareStatement(sql);
      pstmt.setDate(1,StartDate );
      pstmt.setDate(2, EndDate);
      pstmt.setString(3, Slot);
      pstmt.setInt(4, LotID);
      pstmt.setString(5, State);
      pstmt.setInt(6, uid);
      pstmt.executeUpdate(); 
      pstmt.close();
    } catch (SQLException e) {JOptionPane.showMessageDialog(null,e,"DB Connection error",JOptionPane.ERROR_MESSAGE );}
}
}
