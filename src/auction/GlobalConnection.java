/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author nisch
 */
public class GlobalConnection {
    public static final String Url="jdbc:mysql://localhost:3306/auctions"; //here auctions is db name
    public static final String DatabaseUser="root";
    public static final String DatabasePassword="";
   public  Connection conn=null;
    
    public GlobalConnection(){  
        try{
            conn=DriverManager.getConnection(Url,DatabaseUser,DatabasePassword);
        }
          catch(SQLException e){
            JOptionPane.showMessageDialog(null,e,"DB Connection error",JOptionPane.ERROR_MESSAGE );
        }
      
    }
}
