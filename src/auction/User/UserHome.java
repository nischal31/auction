/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package auction.User;

import auction.GlobalConnection;
import Lot.*;
import Bidding.*;
import java.sql.*;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Nischal Prajapati
 */
public class UserHome extends javax.swing.JFrame {
    public static int ItemId;
    Statement st = null;
    PreparedStatement ps = null;
    ResultSet rs = null;    
    UserLoginForm ULF=new UserLoginForm();
    int uid=0;
    GlobalConnection db= new GlobalConnection();
    /**
     * Creates new form UserHome
     */
    public UserHome() {
        initComponents();
        AvailableItemForBid();
        PendingBid();
        WinningBid();
        PendingItem();
        SoldItem();
        setDefaultCloseOperation(UserHome.DISPOSE_ON_CLOSE);
    }
    public void WinningBid(){
            try{
           uid=ULF.UserID;  
           String query=("SELECT LotNo as 'LotNO',Category as 'Category',CurrentBid as 'Highest Bid', StartDate as 'Start Date' , EndDate as 'End Date'  from auction JOIN lot ON auction.AuctionID=lot.AuctionID where auction.UserID='"+uid+"'");
           ps=db.conn.prepareStatement(query);
           rs = ps.executeQuery();
           BidWinTable.setModel(DbUtils.resultSetToTableModel(rs));           
            }
       catch(SQLException e)
       {
           JOptionPane.showMessageDialog(null," Data fetching error" +e);
       }
    }
    public void PendingBid(){
            try{
           uid=ULF.UserID;  
           String query=("SELECT LotNo as 'LotNO',Category as 'Category',CurrentBid as 'Highest Bid', StartDate as 'Start Date' , EndDate as 'End Date'  from auction JOIN lot ON auction.AuctionID=lot.AuctionID where lot.LatestBidBy='"+uid+"' AND auction.State='Assigned'");
           ps=db.conn.prepareStatement(query);
           rs = ps.executeQuery();
           PendingBidTable.setModel(DbUtils.resultSetToTableModel(rs));           
        }
       catch(SQLException e)
       {
           JOptionPane.showMessageDialog(null," Data fetching error" +e);
       }
    }
        public void AvailableItemForBid()
{
    try{  
           String query=("SELECT LotNo as 'LotNO',Category as 'Category',CurrentBid as 'Highest Bid', StartDate as 'Start Date' , EndDate as 'End Date'  from auction JOIN lot ON auction.AuctionID=lot.AuctionID where auction.State='Assigned'");
           ps=db.conn.prepareStatement(query);
           rs = ps.executeQuery();
           AuctionListTable.setModel(DbUtils.resultSetToTableModel(rs));           
        }
       catch(SQLException e)
       {
           JOptionPane.showMessageDialog(null," Data fetching error" +e);
       }
}
         public void PendingItem()
{
    try{  
           String query=("SELECT LotNo as 'LotNO',Category as 'Category',Description as 'Item Name' from lot where State='Unassigned' AND lot.UserID='"+uid+"'");
           ps=db.conn.prepareStatement(query);
           rs = ps.executeQuery();
           PendingItemTable.setModel(DbUtils.resultSetToTableModel(rs));           
        }
       catch(SQLException e)
       {
           JOptionPane.showMessageDialog(null," Data fetching error" +e);
       }
}            
       public void SoldItem(){
            try{
           uid=ULF.UserID;  
           String query=("SELECT LotNo as 'LotNO',Category as 'Category',Description as 'Item Name', StartDate as 'Start Date' , EndDate as 'End Date',CustomerPayment as 'Received Price',Comission as 'Tax'  from auction JOIN lot ON auction.AuctionID=lot.AuctionID where lot.UserID='"+uid+"' AND lot.State='Sold'");
           ps=db.conn.prepareStatement(query);
           rs = ps.executeQuery();
           ItemSoldTable.setModel(DbUtils.resultSetToTableModel(rs));           
        }
       catch(SQLException e)
       {
           JOptionPane.showMessageDialog(null," Data fetching error" +e);
       }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        logout2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblusername2 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        PendingItemTable = new javax.swing.JTable();
        RefreshMyPendingItems = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        ItemSoldTable = new javax.swing.JTable();
        RefreshMySoldItems = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        PendingBidTable = new javax.swing.JTable();
        RefreshMyPendingBid = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        AuctionListTable = new javax.swing.JTable();
        RefreshAuctionItems = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        BidItemButton = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        BidWinTable = new javax.swing.JTable();
        RefreshBiddingWon = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AddLot = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 270, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 34, Short.MAX_VALUE)
        );

        logout2.setText("Logout");
        logout2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logout2ActionPerformed(evt);
            }
        });

        jLabel4.setText("Available items For bidding");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(372, 372, 372)
                .addComponent(lblusername2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(logout2)
                .addContainerGap())
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(718, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logout2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblusername2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 14, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(153, 153, 153), new java.awt.Color(153, 153, 153), new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)));

        PendingItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(PendingItemTable);

        RefreshMyPendingItems.setText("Refresh");
        RefreshMyPendingItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshMyPendingItemsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(57, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(379, 379, 379)
                .addComponent(RefreshMyPendingItems)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(RefreshMyPendingItems)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(73, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("My Pending Items", jPanel6);

        ItemSoldTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(ItemSoldTable);

        RefreshMySoldItems.setText("Refresh");
        RefreshMySoldItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshMySoldItemsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(340, 340, 340)
                        .addComponent(RefreshMySoldItems)))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(RefreshMySoldItems)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(91, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("My Items Sold Details", jPanel7);

        PendingBidTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane6.setViewportView(PendingBidTable);

        RefreshMyPendingBid.setText("Refresh");
        RefreshMyPendingBid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshMyPendingBidActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 800, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(55, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(RefreshMyPendingBid)
                .addGap(261, 261, 261))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(RefreshMyPendingBid)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44))
        );

        jTabbedPane1.addTab("My Pending Bid", jPanel9);

        jPanel5.setBorder(javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(102, 102, 102), new java.awt.Color(153, 153, 153), new java.awt.Color(0, 0, 0), new java.awt.Color(51, 51, 51)), null)));

        AuctionListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        AuctionListTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AuctionListTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(AuctionListTable);

        RefreshAuctionItems.setText("Refresh Auction List");
        RefreshAuctionItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshAuctionItemsActionPerformed(evt);
            }
        });

        jLabel1.setText("Click The Item you want to bidd ");

        BidItemButton.setText("Bid for Item");
        BidItemButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BidItemButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 803, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                        .addComponent(RefreshAuctionItems)
                        .addGap(56, 56, 56)
                        .addComponent(BidItemButton)
                        .addGap(332, 332, 332))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(RefreshAuctionItems)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BidItemButton))
                .addGap(42, 42, 42)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        jTabbedPane1.addTab("Auction Items", jPanel5);

        BidWinTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane5.setViewportView(BidWinTable);

        RefreshBiddingWon.setText("Refresh ");
        RefreshBiddingWon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RefreshBiddingWonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(104, 104, 104)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(237, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(RefreshBiddingWon)
                .addGap(399, 399, 399))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addComponent(RefreshBiddingWon)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );

        jTabbedPane1.addTab("Bidding Won", jPanel8);

        jMenu1.setText("File");

        AddLot.setText("Add Lot");
        AddLot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddLotActionPerformed(evt);
            }
        });
        jMenu1.add(AddLot);

        jMenuItem2.setText("jMenuItem2");
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(626, 626, 626))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTabbedPane1))
                    .addGap(7, 7, 7)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(338, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(124, 124, 124))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(44, 44, 44)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(35, 35, 35)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void logout2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logout2ActionPerformed
        UserLoginForm em =new UserLoginForm();
        em.setVisible(true);
        setVisible(false);
        dispose();
    }//GEN-LAST:event_logout2ActionPerformed

    private void BidItemButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BidItemButtonActionPerformed
        AddBidding ab=new AddBidding();
        try {
            String query = "SELECT LotNo,ReservePrice,EndDate,Description,LotNo FROM lot JOIN auction ON lot.AuctionID=auction.AuctionID WHERE LotNO ='" + ItemId + "'";
            ps = db.conn.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                String LotID=rs.getString("LotNo");
                AddBidding.ItemIDText.setText(LotID);
                String ItemName = rs.getString("Description");
                AddBidding.ItemName.setText(ItemName);
                String EndDate = rs.getString("EndDate");
                AddBidding.EndDate.setText(EndDate);
                AddBidding.ReservePriceText.setText(rs.getString("ReservePrice"));
                ab.setVisible(true);
                setVisible(false);
            }
        }
        catch(SQLException e){
            System.out.println("query error"+e);
        }
    }//GEN-LAST:event_BidItemButtonActionPerformed

    private void AuctionListTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AuctionListTableMouseClicked
         int row = AuctionListTable.getSelectedRow();
        String Table_click = (AuctionListTable.getModel().getValueAt(row, 0).toString());
        ItemId=Integer.parseInt(Table_click);
    }//GEN-LAST:event_AuctionListTableMouseClicked

    private void AddLotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddLotActionPerformed
        new AddLot().setVisible(true);
    }//GEN-LAST:event_AddLotActionPerformed

    private void RefreshMySoldItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshMySoldItemsActionPerformed
             SoldItem();
    }//GEN-LAST:event_RefreshMySoldItemsActionPerformed

    private void RefreshMyPendingBidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshMyPendingBidActionPerformed
            PendingBid();
    }//GEN-LAST:event_RefreshMyPendingBidActionPerformed

    private void RefreshAuctionItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshAuctionItemsActionPerformed
             AvailableItemForBid();
    }//GEN-LAST:event_RefreshAuctionItemsActionPerformed

    private void RefreshBiddingWonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshBiddingWonActionPerformed
            WinningBid();
    }//GEN-LAST:event_RefreshBiddingWonActionPerformed

    private void RefreshMyPendingItemsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RefreshMyPendingItemsActionPerformed
             PendingItem();
    }//GEN-LAST:event_RefreshMyPendingItemsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UserHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UserHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UserHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UserHome.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if(UserAccess.getUserLogin()==true){
                new UserHome().setVisible(true);    
                }
                else{
                    new UserLoginForm().setVisible(true);
                }
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AddLot;
    private javax.swing.JTable AuctionListTable;
    private javax.swing.JButton BidItemButton;
    private javax.swing.JTable BidWinTable;
    private javax.swing.JTable ItemSoldTable;
    private javax.swing.JTable PendingBidTable;
    private javax.swing.JTable PendingItemTable;
    private javax.swing.JButton RefreshAuctionItems;
    private javax.swing.JButton RefreshBiddingWon;
    private javax.swing.JButton RefreshMyPendingBid;
    private javax.swing.JButton RefreshMyPendingItems;
    private javax.swing.JButton RefreshMySoldItems;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    public static javax.swing.JLabel lblusername2;
    private javax.swing.JButton logout2;
    // End of variables declaration//GEN-END:variables
}
